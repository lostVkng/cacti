/* 
    Cron Job
*/

import * as server from './server';



export class CronJob {
    
    name: string;
    args: Array<string>;
    cronArgs: string;
    timer: any;
    lastRun: string;
    nextRun: string;
    constructor(args: Array<string>, cronArgs: string, name?:string) {
        //super();

        // name exists?
        if(name) {
            this.name = name;
        } else {
            let filename: string = args[0].replace(/^.*[\\\/]/, '');
            this.name = filename;
        }

        this.args = args;
        this.cronArgs = cronArgs;

        // run timestamps
        this.lastRun = '';
        this.nextRun = '';

        // set initial timer
        this.startTimer();


    }

    startTimer() {
        
        // determine next time to run
        let currentTime:Date = new Date();
        let nextRunDate:Date = findNextDate(this.cronArgs);

        // time between next run date & now in milliseconds
        let diffMS:any = nextRunDate.getTime() - currentTime.getTime();

        let self = this;

        // create timer: diffMS
        this.timer = setTimeout(() => {
            self.run();
        }, diffMS);

        // update next run time
        this.nextRun = nextRunDate.toLocaleString();
    }

    stop() {

        clearTimeout(this.timer);
    }

    run() {

        // restart interval
        this.startTimer();

        // tell server to start job
        server.start(this.args, this.name, true, undefined);

        // update last run time
        this.lastRun = new Date().toLocaleString();

    }
}

// minute hour day(month) month day(week)

// validate user provided cron schedule
export function validateSchedule(str: string) {
    
    // spit time periods
    let arr = str.split(' ');

    try {

        // minute
        validateExpression(arr[0], 59);

        // hour
        validateExpression(arr[1], 23);

        // day of month
        validateExpression(arr[2], 30);

        // month
        validateExpression(arr[3], 11);

        // day of week
        validateExpression(arr[4], 6);
        
    } catch (error) {
        throw error;
    }
    
    return true;

}


// * any value
// , value seperator
// - range of values
// / step values
function validateExpression(exp: string, max: number): Array<number> {

    // convert expression to explicit values as array:
    let arr: Array<number> = []

    try {
        // convert expression to array of possibilities
        if(exp.includes('*')) {
            arr = convAstrix(exp, max);
        } else if(exp.includes('-')) {
            arr = convRange(exp);
        } else if(exp.includes('/')) {
            arr = convStep(exp, max);
        } else {
            arr = exp.split(',').map((x:string) => parseInt(x));
        }
        
    } catch (error) {
        // TODO: error log
        console.log('error: ');
        console.log(error);
        throw error;
    }

    // bad value catch
    if(arr.some((v:any) => isNaN(v))) throw 'value is NaN';

    if(arr.some((v:any) => v > max)) throw 'value > max';
    if(arr.some((v:any) => v < 0)) throw 'value < 0';

    return arr;
}

function convAstrix(exp: string, max: number) {

    let arr =  [...Array(max+1).keys()]

    // astrix w/ step
    if(exp.includes('/')) {
        let step:number = parseInt(exp.split('/')[1]);
        
        // remove items not on step
        arr = arr.filter((val, i) => (i % step) == 0)
    }

    return arr;
}

function convRange(exp: string) {

    // check if step is included
    let stepSplit = exp.split('/');
    let step = parseInt(stepSplit[1]) ? parseInt(stepSplit[1]) : 1;

    // isolate start & finish
    let rangeSplit = stepSplit[0].split('-');
    let start = parseInt(rangeSplit[0]);
    let stop = parseInt(rangeSplit[1]);

    // throw error for NaN
    if(isNaN(start) || isNaN(stop)) throw 'Range with NaN';

    // populate response
    let arr = [];
    let _i = start;
    while(_i <= stop) {
        arr.push(_i);
        _i += step;
    }

    return arr;
}

// step that doesn't include range or *
function convStep(exp: string, max: number) {

    let stepSplit = exp.split('/');
    let  step = parseInt(stepSplit[1]);

    let arr = [];
    let _i = parseInt(stepSplit[0]);
    while(_i <= max) {
        arr.push(_i);
        _i += step;
    }

    return arr;
}

function findNextDate(cronArgs: string) {

    // this stackoverflow logic is great
    // had to modify for different ranges and JS
    // https://stackoverflow.com/questions/321494/calculate-when-a-cron-job-will-be-executed-then-next-time

    // current date
    let next = new Date();

    // increment now by 1 min to ensure date isn't now
    next.setTime(next.getTime() + 1000 * 60);
    
    // always set seconds to 0
    next.setSeconds(0);

    // array of potential time period values
    let _cronArgsSplit = cronArgs.split(' ');
    let cron = {
        min: validateExpression(_cronArgsSplit[0], 59),
        hour: validateExpression(_cronArgsSplit[1],23),
        day: validateExpression(_cronArgsSplit[2],30),
        month: validateExpression(_cronArgsSplit[3],11),
        weekday: validateExpression(_cronArgsSplit[4],6),
    };
    
    // Loop to find next possible date
    /*
        Logic: increment potential date until it matches potential cron values
    */
    while(true) {

        // Month
        if(!cron.month.includes(next.getMonth())) {
            next.setDate(cron.day[0]);
            next.setHours(cron.hour[0]);
            next.setMinutes(cron.min[0]);
            next.setMonth(next.getMonth()+1);
            continue;
        }
        // Day
        if(!cron.day.includes(next.getDate())) {
            next.setHours(cron.hour[0]);
            next.setMinutes(cron.min[0]);
            next.setDate(next.getDate()+1);
            continue;
        }
        // Weekday
        if(!cron.weekday.includes(next.getDay())) {
            next.setHours(cron.hour[0]);
            next.setMinutes(cron.min[0]);
            next.setDate(next.getDate()+1);
            continue;
        }
        // Hour
        if(!cron.hour.includes(next.getHours())) {
            next.setMinutes(cron.min[0]);
            next.setHours(next.getHours()+1);
            continue;
        }
        // Minute
        if(!cron.min.includes(next.getMinutes())) {
            next.setMinutes(next.getMinutes()+1);
            continue;
        }
        break;
    }

    return next;
}

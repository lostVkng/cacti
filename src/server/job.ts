/* 
    Job
*/

import { spawn, exec } from 'child_process';
import { EventEmitter } from 'events';


export class Job extends EventEmitter {
    
    name: string;
    proc: any;
    args: Array<string>;
    pid: number;
    cron: boolean;
    forever: boolean;

    constructor({name, cron, forever, args}:{name?:string, cron?:boolean, forever?:boolean, args:Array<string>}) {
        super();

        // name exists?
        if(name) {
            this.name = name;
        } else {
            let filename: string = args[0].replace(/^.*[\\\/]/, '');
            this.name = filename;
        }

        // job attributes
        this.cron = cron ? cron : false;
        this.forever = forever ? forever : false;
        this.args = args;
        this.pid = 0;

        // start the job
        this.start();
        
    }

    start() {
        
        try {
            // create process
            this.proc = spawn(this.args[0], this.args.slice(1));
            this.pid = this.proc.pid;
            let self = this;

            // process events
            this.proc.stdout.on('data', (d:any) => {
                //console.log(d.toString());
            });

            // error on job, stop
            this.proc.stderr.on('data', (err:any) => {

                // if there is an error, exit the job
                self.stop();
              });

            // exit event
            this.proc.on('exit', (e:any) => {

                // restart exit
                if(self.forever) {
                    self.start();
                } else {
                    self.emit('exit');
                }
            }) 

            // spawn error
            this.proc.on('error', (e:any) => {
                self.stop();
            }) 
        } catch (error) {
            console.log(error)
        }
        
    }

    stop() {

        // set forever attribute to prevent restarts
        this.forever = false;

        // kill the process
        this.proc.kill();

        // emit exit to let cacti know its not alive
        this.emit('exit');
    }
}

export interface pidStats {
    mem: number,
    elapsed: number,
    cpu: number
}

export async function getJobInfo(pid:number):Promise<pidStats> {
    
    let cmd = 'ps -o pid,rss,pcpu,etime -p '+pid    

    return new Promise((resolve, reject) => { 

        exec(cmd, (error:any, stdout:any, stderr:any) => {

            // throw non stdout
            if(!stdout) reject('error no PID data');

            // split data
            /*
                ps command output:
                PID    RSS   %CPU   ELAPSED
                22586  8360  0.5    00:02

                split output:
                ['PID','RSS','%CPU','ELAPSED',
                 '22586','8360', '0.5','00:02']

            */
            let line = stdout.trim().split(/\s+/);

            let res:pidStats = {
                'cpu': parseInt(line[6]),
                'mem': parseInt(line[5]) * 1024,
                'elapsed': parseTime(line[7])
            }

            resolve(res);
        });
    });
    
}


function parseTime (timestamp:string) {
    
    // timestamp is in seconds
    let ms:number = 0;
    let split = timestamp.split(/-|:|\./);
    
    // days
    if (split.length > 3) {
        ms += parseInt(split[0]) * 86400000;
        split.shift();
    }
    
    // hours
    if (split.length > 2) {
        ms += parseInt(split[0]) * 3600000;
        split.shift();
    }
    
    // minutes
    if (split.length > 1) {
        ms += parseInt(split[0]) * 60000;
        split.shift();
    }

    // seconds
    if (split.length > 0) {
        ms += parseInt(split[0]) * 1000;
        split.shift();
    }

    return ms
  }
/* 

*/

import net = require('net');

import {JsonWire} from './json_wire'; 
import {Job, getJobInfo, pidStats} from './job';
import * as pro from '../cli/protocol';
import * as cron from './cron';


// Store stems
let CactiJobs: Array<Job> = [];
let CronTasks: Array<cron.CronJob> = [];

process.title = 'Cacti Server';

// Start the tcp server
export function startServer() {

    // create tcp server
    let server = net.createServer();

    server.on('connection', (sock: any) => {

        sock = new JsonWire(sock);

        sock.on('data', async (data: pro.ServerReq) => {

            // construct response object
            let resp: pro.ServerRes = {
                command: data.command,
                args: data.args,
            };

            try {

                // cli commands
                switch(data.command) {
                    case 'list':
                        resp.data = await list(); 
                        break;
                    case 'start':
                        resp.data = await start(data.args, data.name, false, data.forever);
                        break;
                    case 'stop':

                        if(!data.pid) throw 'No PID included';

                        resp.data = await stop(data.pid);
                        
                        break;
                    case 'add':

                        if(!data.cron_args) throw 'No Cron String included';

                        resp.data = await add(data.args, data.cron_args, data.name);
                        
                        break;
                    case 'remove':

                        if(!data.name) throw 'remove requires a name';

                        resp.data = await remove(data.name)
                                       
                        break;
                    default:
                        throw 'command not recognized'
                }

                // send the response back
                sock.write(resp);
                
            } catch (error) {
                resp.error = error;
                sock.write(resp);
            }

        });

        sock.on('error', (err:any) => {
            console.log('server err')
            console.log(err);
        });
    });

    server.listen(8127, '127.0.0.1', () => {

        console.log('TCP Server is running on port ' + 8127);

    });

}

async function list() {

    interface resData {
        active: Array<pro.JobStatusProtocol>,
        scheduled: Array<pro.CronJobProtocol> 
    };

    let stemList: Array<pro.JobStatusProtocol> = [];

    // loop through active server stems
    var exec = require('child_process').exec
    for(let s of CactiJobs) {
        
        // get job stats
        let stats:pidStats = await getJobInfo(s.pid);

        // construct response
        let statusRes: pro.JobStatusProtocol = {
            name: s.name,
            cron: s.cron,
            mem: Math.round(stats.mem / 1048576),
            cpu: stats.cpu,
            time_up: stats.elapsed,
            pid: s.pid,
            forever: s.forever
        };

        stemList.push(statusRes);
    }

    // loop through scheduled
    let scheduledList: Array<pro.CronJobProtocol> = [];

    for(let c of CronTasks) {
        
        // construct response
        let statusRes: pro.CronJobProtocol = {
            name: c.name,
            cronString: c.cronArgs,
            lastRun: c.lastRun,
            nextRun: c.nextRun,
        };

        scheduledList.push(statusRes);
    }

    let data:resData = {
        active: stemList,
        scheduled: scheduledList,
    };

    return data;
}

// Start a new Job
export async function start(args: Array<string>, name?:string, cron?: boolean, forever?:boolean) {

    try {
        let s = new Job({name:name, args:args, cron:cron, forever:forever});

        // listen for exit to remove from array
        s.on('exit', () => {
            let i = CactiJobs.findIndex(x => x.pid === s.pid);
    
            // remove job
            CactiJobs.splice(i, 1);
        })
    
        CactiJobs.push(s);

    } catch (error) {

        throw error;
    }
   
}

// Stop a running Job
async function stop(pid:number) {

    // find job
    let i = CactiJobs.findIndex(x => x.pid === pid);

    if (i == -1) throw 'PID not valid';

    // stop the job
    let job = CactiJobs[i]
    job.stop();
    
}

// Add a cron job
async function add(args: Array<string>, cronArgs: string, name?:string)  {

    try {

        // validate cron args
        cron.validateSchedule(cronArgs);

        // create cron job
        let j = new cron.CronJob(args, cronArgs, name);

        CronTasks.push(j);
        
    } catch (error) {

        throw error;
    }
}

// Remove a cron job
async function remove(name:string)  {

    try {

        // find cron job
        let i = CronTasks.findIndex(x => x.name === name);

        // stop the job
        CronTasks[i].stop();

        // remove job
        CronTasks.splice(i, 1);
        
    } catch (error) {

        throw error;
    }
}
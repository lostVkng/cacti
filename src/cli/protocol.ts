/*

    Protocol for sending messages back and forth between server and client
*/

/* 
    Stem data representation
*/
export interface JobStatusProtocol {
    name: string,
    mem: number,
    cpu: number,
    pid: number,
    cron: boolean,
    time_up: number,
    forever: boolean,
}

/* 
    Scheduled data representation
*/
export interface CronJobProtocol {
    name: string,
    cronString: string,
    lastRun: string,
    nextRun: string,
}

/* 
    Server/Client IO protocols
*/

/*
    Object to send to server
*/
export interface ServerReq {
    command: string,
    args: Array<string>,
    name?: string,
    forever?: boolean,
    cron_args?: string,
    pid?: number,
}

/* 
    Server response to client
        - basically same as req, maybe change in future
*/
export interface ServerRes {
    command: string,
    args: Array<string>,
    data?: any,
    error?: any,
}
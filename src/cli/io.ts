/*

    Clean input and output
    --> Basically the CLI client
*/

import * as pro from './protocol';
import {Client} from './client';
import * as ascii from './UI/ascii_table';

/* 
    Formatting code
*/
export async function list() {

    try {

        // establish client
        let _client: Client = new Client();

        // craft message
        let msg: pro.ServerReq = {command: 'list', args: []};

        // send message
        let res: pro.ServerRes = await _client.sendMsg(msg);

        // construct terminal
        let terminalString: string = '';
        let termWidth = process.stdout.columns;

        // active stems section
        let headers = ['name', 'cpu', 'mem', 'pid', 'time up', 'cron', 'forever'];
        let rows = res.data.active.map((x:any, i:number) => {
            return [
                x.name.toString(), 
                Math.round(x.cpu)+'%', x.mem+' MB', 
                x.pid.toString(), 
                timeString(x.time_up), 
                x.cron.toString(),
                x.forever.toString()
            ];
        });

        terminalString += '\nActive Stems \n';
        terminalString += ascii.table(headers, rows);

        // scheduled stems section
        let sHeaders = ['name', 'cronString', 'lastRun', 'nextRun'];
        let sRows = res.data.scheduled.map((x:any, i:number) => {
            return [
                x.name.toString(), 
                x.cronString,
                x.lastRun,
                x.nextRun,
            ];
        });

        terminalString += '\nScheduled Stems \n';
        terminalString += ascii.table(sHeaders, sRows);

        console.log(terminalString);
        
    } catch (error) {
        console.log(error);
    }
}

export async function start(args:Array<string>, name?:string, forever?:boolean) {

    try {
        // establish client
        let _client: Client = new Client();

        // craft message
        let msg: pro.ServerReq = {command: 'start', args: args, name:name, forever:forever};

        // send message
        _client.sendMsg(msg)
            .then((data) => {
                if(data.error) errConsole(data.error);
                if(data.data) console.log(data);
            })
            .catch((err) => errConsole(err));
            
    } catch (err) {
        console.log(err);
    }
}

export async function stop(pid:number) {

    try {
        // establish client
        let _client: Client = new Client();

        // craft message
        let msg: pro.ServerReq = {command: 'stop', args: [], pid:pid};

        // send message
        _client.sendMsg(msg)
            .then((data) => {
                if(data.error) errConsole(data.error);
                if(data.data) console.log(data);
            })
            .catch((err) => errConsole(err));
            
    } catch (err) {
        console.log(err);
    }
}

export async function add(cronArgs: string, args:Array<string>, name?:string) {

    try {
        // establish client
        let _client: Client = new Client();

        // craft message
        let msg: pro.ServerReq = {command: 'add', args: args, name:name, cron_args:cronArgs};

        // send message
        _client.sendMsg(msg)
            .then((data) => {
                if(data.error) errConsole(data.error);
                if(data.data) console.log(data);
            })
            .catch((err) => errConsole(err));
            
    } catch (err) {
        console.log(err);
    }
}

export async function remove(name:string) {

    try {
        // establish client
        let _client: Client = new Client();

        // craft message
        let msg: pro.ServerReq = {command: 'remove', name:name, args:[]};

        // send message
        _client.sendMsg(msg)
            .then((data) => {
                if(data.error) errConsole(data.error);
                if(data.data) console.log(data);
            })
            .catch((err) => errConsole(err));
            
    } catch (err) {
        console.log(err);
    }
}



export async function help() {

    let s: string = '';

    // title
    s += 'Usage: cacti <command> [optional flags] [arguments]';
    s += '\n\n';

    // commands
    s += 'cacti <server>    | launch cacti server\n';
    s += '\n';
    s += 'cacti <list>      | lists active and scheduled jobs\n';
    s += '\n';
    s += 'cacti <start> [shell arguments]     | start job\n';
    s += 'optional flags:\n';
    s += '    -name=[name]    | name to identify job\n';
    s += '    -forever        | relaunch job if it exits\n';
    s += '\n';
    s += 'cacti <stop> [pid]     | stop job\n';
    s += '\n';
    s += 'cacti <add> [cron string] [shell arguments]     | add cron job\n';
    s += 'optional flags:\n';
    s += '    -name=[name]    | name to identify job\n';
    s += '\n';
    s += 'cacti <remove> [name]     | remove cron job';
    s += '\n';

    console.log(s);
}



/* 
    Helper functions
*/
function errConsole(err: string) {
    console.log(err);
}

function timeString(ms:number):string {

    if(isNaN(ms)) return 'NaN';

    if(ms < 1000) {
        return Math.round(ms)+'ms';
    } else if(ms < 60000) {
        return Math.round(ms / 1000)+'s';
    } else if(ms <3600000) {
        return Math.round(ms / 60000)+'m';
    } else if(ms <86400000) {
        return Math.round(ms / 3600000)+'h';
    } else {
        return Math.round(ms / 86400000)+'d';
    }
}



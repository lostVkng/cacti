/* 
    tcp client

    this is mainly just a wrapper to not have to write this over and over
*/

import {JsonWire} from '../server/json_wire'; 
import * as pro from './protocol';


export class Client {

    socket: any;
    constructor() {

        // create socket connection
        this.socket = new JsonWire();
        this.socket.connect({ host: '127.0.0.1', port: 8127 });

    }

    sendMsg(msg: pro.ServerReq): Promise<pro.ServerRes> {

        let _client = this.socket;

        return new Promise((resolve, reject) => { 
            
            // send message to server
            _client.write(msg);

            // wait for data
            _client.on('data', (res: pro.ServerRes) => {

                // close the connection
                _client.end();

                // send data back
                resolve(res);
            });

            // or error
            _client.on('error', (err:any) => {
                
                // close the connection
                _client.end();

                // send data back
                reject(err);
            });

        });
    }

    stream() {
        
    }
}
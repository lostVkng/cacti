#!/usr/bin/env node

/*

*/

import * as server from './server/server';
import * as io from './cli/io';

/* 
-cron=[]
-forever=true
-name=string

start: start a job or alive job
kill: kill any type of job
add: create a cron job
remove: remove a cron job
killall: stop cacti and all jobs

*/

function main() {

    let args = process.argv.slice(2);

    // match args
    switch(args[0]) {
        case 'server':
            
            // start socket server
            server.startServer();

            break;
        case 'list':
            io.list()
            break;
        
        // start a task
        case 'start': {

            // check if args includes name and forever
            let name, forever;

            let iName = args.findIndex((v:string) => v.includes('-name='));
            if(iName !== -1) {
                name = args[iName].slice(6);
                args.splice(iName,1);
            }

            let iForever = args.findIndex((v:string) => v.includes('-forever'));
            if(iForever !== -1) {
                forever = true;
                args.splice(iForever,1);
            }

            // drop start
            args.splice(0,1);

            io.start(args, name, forever);
            break;
        }
        case 'stop': {
            
            io.stop(parseInt(args[1]));
            break;
        }
        case 'add': {

            let name;

            let iName = args.findIndex((v:string) => v.includes('-name='));
            if(iName !== -1) {
                name = args[iName].slice(6);
                args.splice(iName,1);
            }

            // cron args should be [1]
            let cronArgs = args[1];

            // drop add & cronargs
            args.splice(0,2);

            io.add(cronArgs, args, name);
            break;
        }
        case 'remove': {
           
            let name = args[1];

            io.remove(name);            
            break;
        }
        case '--version':
            let pjson = require('../package.json');
            console.log(pjson.version);
            break
        case '--help':
            io.help();
            break;
        default:
            console.log('args no make sense');
    }

};

main();
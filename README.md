# Cacti

process management in a non-process driven world

I wrote cacti to create a zero dependency process management app for my raspberry pi. The main concept is that it runs shell commands in the background. Cacti supports cron syntax and forever flag which relauches processes when they crash.

# Installation

Install cacti globally with npm
```shell
npm i -g @lostvkng/cacti
```

Attach cacti to systemd/systemctl/etc
See daemons dir for examples


# Usage

```shell
cacti list
```
Lists all active and scheduled jobs

```shell
cacti start [shell arguements]

options:
- name=[name] | custom job name
- forever     | relaunch job on exit

ex:
cacti start python3 /home/user/test.py
cacti start -name="blah" python3 /home/user/test.py
cacti start python3 /home/user/test.py -forever
```
Start a new job. You can set a unique name yourself. Adding the ```forever``` flag ensures the process restarts on exit.

```shell
cacti stop [pid]

ex:
cacti stop 12509
```
Kills an active job based on the pid. Will also kill any job launched with forever flag.

```shell
cacti add [cron string] [shell arguments]

options:
- name=[name] | custom job name

ex:
cacti add '* 19 * * *' python3 /home/user/Desktop/test.py
cacti add '* */2 3 * *' -name=notifyME python3 /home/user/Desktop/test.py
```
Add a new cron job to cacti. This launches shell code on set intervals. The cron syntax is further documented below.

```shell
cacti remove [name]

ex:
cacti remove notifyME
```
Removes a cron job by the assigned name.

```shell
cacti server
```
Launch the cacti server, this is usually called by the systemd/systemctl. Cacti communicates with a bare minimum node.js server over tcp on port 8127



# Cron syntax
Cron syntax allows us to specify when to launch a task with only a space seperated string.

The syntax is seperated as so:
```
minute hour day(month) month day(week)

* any value
, value list seperator
- range of values
/ step values
```

### Examples:
```
'* * * * *'
```
Run every minute/hour/day/month/day of week

```
'5 4 * * *'
```
Run every 4th hour and 5th minute for every day/month/day of week

```
'0 22 * * 1-5'
```
Run at 22h00 every weekday

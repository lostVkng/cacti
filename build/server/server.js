"use strict";
/*

*/
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.start = exports.startServer = void 0;
const net = require("net");
const json_wire_1 = require("./json_wire");
const job_1 = require("./job");
const cron = __importStar(require("./cron"));
// Store stems
let CactiJobs = [];
let CronTasks = [];
process.title = 'Cacti Server';
// Start the tcp server
function startServer() {
    // create tcp server
    let server = net.createServer();
    server.on('connection', (sock) => {
        sock = new json_wire_1.JsonWire(sock);
        sock.on('data', async (data) => {
            // construct response object
            let resp = {
                command: data.command,
                args: data.args,
            };
            try {
                // cli commands
                switch (data.command) {
                    case 'list':
                        resp.data = await list();
                        break;
                    case 'start':
                        resp.data = await start(data.args, data.name, false, data.forever);
                        break;
                    case 'stop':
                        if (!data.pid)
                            throw 'No PID included';
                        resp.data = await stop(data.pid);
                        break;
                    case 'add':
                        if (!data.cron_args)
                            throw 'No Cron String included';
                        resp.data = await add(data.args, data.cron_args, data.name);
                        break;
                    case 'remove':
                        if (!data.name)
                            throw 'remove requires a name';
                        resp.data = await remove(data.name);
                        break;
                    default:
                        throw 'command not recognized';
                }
                // send the response back
                sock.write(resp);
            }
            catch (error) {
                resp.error = error;
                sock.write(resp);
            }
        });
        sock.on('error', (err) => {
            console.log('server err');
            console.log(err);
        });
    });
    server.listen(8127, '127.0.0.1', () => {
        console.log('TCP Server is running on port ' + 8127);
    });
}
exports.startServer = startServer;
async function list() {
    ;
    let stemList = [];
    // loop through active server stems
    var exec = require('child_process').exec;
    for (let s of CactiJobs) {
        // get job stats
        let stats = await job_1.getJobInfo(s.pid);
        // construct response
        let statusRes = {
            name: s.name,
            cron: s.cron,
            mem: Math.round(stats.mem / 1048576),
            cpu: stats.cpu,
            time_up: stats.elapsed,
            pid: s.pid,
            forever: s.forever
        };
        stemList.push(statusRes);
    }
    // loop through scheduled
    let scheduledList = [];
    for (let c of CronTasks) {
        // construct response
        let statusRes = {
            name: c.name,
            cronString: c.cronArgs,
            lastRun: c.lastRun,
            nextRun: c.nextRun,
        };
        scheduledList.push(statusRes);
    }
    let data = {
        active: stemList,
        scheduled: scheduledList,
    };
    return data;
}
// Start a new Job
async function start(args, name, cron, forever) {
    try {
        let s = new job_1.Job({ name: name, args: args, cron: cron, forever: forever });
        // listen for exit to remove from array
        s.on('exit', () => {
            let i = CactiJobs.findIndex(x => x.pid === s.pid);
            // remove job
            CactiJobs.splice(i, 1);
        });
        CactiJobs.push(s);
    }
    catch (error) {
        throw error;
    }
}
exports.start = start;
// Stop a running Job
async function stop(pid) {
    // find job
    let i = CactiJobs.findIndex(x => x.pid === pid);
    if (i == -1)
        throw 'PID not valid';
    // stop the job
    let job = CactiJobs[i];
    job.stop();
}
// Add a cron job
async function add(args, cronArgs, name) {
    try {
        // validate cron args
        cron.validateSchedule(cronArgs);
        // create cron job
        let j = new cron.CronJob(args, cronArgs, name);
        CronTasks.push(j);
    }
    catch (error) {
        throw error;
    }
}
// Remove a cron job
async function remove(name) {
    try {
        // find cron job
        let i = CronTasks.findIndex(x => x.name === name);
        // stop the job
        CronTasks[i].stop();
        // remove job
        CronTasks.splice(i, 1);
    }
    catch (error) {
        throw error;
    }
}

"use strict";
/*
    json wire

    custom json wire protocol to send json data over tcp streams

    Header   |  Payload - json body
    4 bytes  |

    // I took majority from this blog post, its an excellent read
    // http://derpturkey.com/extending-tcp-socket-in-node-js/
*/
Object.defineProperty(exports, "__esModule", { value: true });
exports.JsonWire = void 0;
const stream_1 = require("stream");
const net_1 = require("net");
class JsonWire extends stream_1.Duplex {
    constructor(socket) {
        super({ objectMode: true });
        // used to control reading
        this._readingPaused = false;
        // wrap the socket if one was provided
        if (socket)
            this._wrapSocket(socket);
    }
    connect(server) {
        this._wrapSocket(new net_1.Socket());
        let s = {
            host: server.host,
            port: server.port
        };
        this._socket.connect(s);
        return this;
    }
    _wrapSocket(socket) {
        this._socket = socket;
        // pass through methods
        this._socket.on('connect', () => this.emit('connect'));
        this._socket.on('drain', () => this.emit('drain'));
        this._socket.on('end', () => this.emit('end'));
        this._socket.on('error', (err) => this.emit('error', err));
        this._socket.on('lookup', (err, address, family, host) => this.emit('lookup', err, address, family, host));
        this._socket.on('ready', () => this.emit('ready'));
        this._socket.on('timeout', () => this.emit('timeout'));
        // tbd
        this._socket.on('readable', this._onReadable.bind(this));
    }
    _onReadable() {
        while (!this._readingPaused) {
            // read raw length
            let lenBuf = this._socket.read(4);
            if (!lenBuf)
                return;
            let len = lenBuf.readUInt32BE();
            // read json data
            let body = this._socket.read(len);
            if (!body) {
                this._socket.unshift(lenBuf);
                return;
            }
            // convert raw json to js object
            let json;
            try {
                json = JSON.parse(body);
            }
            catch (ex) {
                this._socket.destroy(ex);
                return;
            }
            // add object to read buffer
            let pushOk = this.push(json);
            // pause reading
            if (!pushOk)
                this._readingPaused = true;
        }
    }
    _read() {
        this._readingPaused = false;
        setImmediate(this._onReadable.bind(this));
    }
    _write(obj, encoding, cb) {
        let json = JSON.stringify(obj);
        let jsonBytes = Buffer.byteLength(json);
        let buffer = Buffer.alloc(4 + jsonBytes);
        buffer.writeUInt32BE(jsonBytes);
        buffer.write(json, 4);
        this._socket.write(buffer, encoding, cb);
    }
    _final(cb) {
        this._socket.end(cb);
    }
}
exports.JsonWire = JsonWire;

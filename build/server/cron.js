"use strict";
/*
    Cron Job
*/
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateSchedule = exports.CronJob = void 0;
const server = __importStar(require("./server"));
class CronJob {
    constructor(args, cronArgs, name) {
        //super();
        // name exists?
        if (name) {
            this.name = name;
        }
        else {
            let filename = args[0].replace(/^.*[\\\/]/, '');
            this.name = filename;
        }
        this.args = args;
        this.cronArgs = cronArgs;
        // run timestamps
        this.lastRun = '';
        this.nextRun = '';
        // set initial timer
        this.startTimer();
    }
    startTimer() {
        // determine next time to run
        let currentTime = new Date();
        let nextRunDate = findNextDate(this.cronArgs);
        // time between next run date & now in milliseconds
        let diffMS = nextRunDate.getTime() - currentTime.getTime();
        let self = this;
        // create timer: diffMS
        this.timer = setTimeout(() => {
            self.run();
        }, diffMS);
        // update next run time
        this.nextRun = nextRunDate.toLocaleString();
    }
    stop() {
        clearTimeout(this.timer);
    }
    run() {
        // restart interval
        this.startTimer();
        // tell server to start job
        server.start(this.args, this.name, true, undefined);
        // update last run time
        this.lastRun = new Date().toLocaleString();
    }
}
exports.CronJob = CronJob;
// minute hour day(month) month day(week)
// validate user provided cron schedule
function validateSchedule(str) {
    // spit time periods
    let arr = str.split(' ');
    try {
        // minute
        validateExpression(arr[0], 59);
        // hour
        validateExpression(arr[1], 23);
        // day of month
        validateExpression(arr[2], 30);
        // month
        validateExpression(arr[3], 11);
        // day of week
        validateExpression(arr[4], 6);
    }
    catch (error) {
        throw error;
    }
    return true;
}
exports.validateSchedule = validateSchedule;
// * any value
// , value seperator
// - range of values
// / step values
function validateExpression(exp, max) {
    // convert expression to explicit values as array:
    let arr = [];
    try {
        // convert expression to array of possibilities
        if (exp.includes('*')) {
            arr = convAstrix(exp, max);
        }
        else if (exp.includes('-')) {
            arr = convRange(exp);
        }
        else if (exp.includes('/')) {
            arr = convStep(exp, max);
        }
        else {
            arr = exp.split(',').map((x) => parseInt(x));
        }
    }
    catch (error) {
        // TODO: error log
        console.log('error: ');
        console.log(error);
        throw error;
    }
    // bad value catch
    if (arr.some((v) => isNaN(v)))
        throw 'value is NaN';
    if (arr.some((v) => v > max))
        throw 'value > max';
    if (arr.some((v) => v < 0))
        throw 'value < 0';
    return arr;
}
function convAstrix(exp, max) {
    let arr = [...Array(max + 1).keys()];
    // astrix w/ step
    if (exp.includes('/')) {
        let step = parseInt(exp.split('/')[1]);
        // remove items not on step
        arr = arr.filter((val, i) => (i % step) == 0);
    }
    return arr;
}
function convRange(exp) {
    // check if step is included
    let stepSplit = exp.split('/');
    let step = parseInt(stepSplit[1]) ? parseInt(stepSplit[1]) : 1;
    // isolate start & finish
    let rangeSplit = stepSplit[0].split('-');
    let start = parseInt(rangeSplit[0]);
    let stop = parseInt(rangeSplit[1]);
    // throw error for NaN
    if (isNaN(start) || isNaN(stop))
        throw 'Range with NaN';
    // populate response
    let arr = [];
    let _i = start;
    while (_i <= stop) {
        arr.push(_i);
        _i += step;
    }
    return arr;
}
// step that doesn't include range or *
function convStep(exp, max) {
    let stepSplit = exp.split('/');
    let step = parseInt(stepSplit[1]);
    let arr = [];
    let _i = parseInt(stepSplit[0]);
    while (_i <= max) {
        arr.push(_i);
        _i += step;
    }
    return arr;
}
function findNextDate(cronArgs) {
    // this stackoverflow logic is great
    // had to modify for different ranges and JS
    // https://stackoverflow.com/questions/321494/calculate-when-a-cron-job-will-be-executed-then-next-time
    // current date
    let next = new Date();
    // increment now by 1 min to ensure date isn't now
    next.setTime(next.getTime() + 1000 * 60);
    // always set seconds to 0
    next.setSeconds(0);
    // array of potential time period values
    let _cronArgsSplit = cronArgs.split(' ');
    let cron = {
        min: validateExpression(_cronArgsSplit[0], 59),
        hour: validateExpression(_cronArgsSplit[1], 23),
        day: validateExpression(_cronArgsSplit[2], 30),
        month: validateExpression(_cronArgsSplit[3], 11),
        weekday: validateExpression(_cronArgsSplit[4], 6),
    };
    // Loop to find next possible date
    /*
        Logic: increment potential date until it matches potential cron values
    */
    while (true) {
        // Month
        if (!cron.month.includes(next.getMonth())) {
            next.setDate(cron.day[0]);
            next.setHours(cron.hour[0]);
            next.setMinutes(cron.min[0]);
            next.setMonth(next.getMonth() + 1);
            continue;
        }
        // Day
        if (!cron.day.includes(next.getDate())) {
            next.setHours(cron.hour[0]);
            next.setMinutes(cron.min[0]);
            next.setDate(next.getDate() + 1);
            continue;
        }
        // Weekday
        if (!cron.weekday.includes(next.getDay())) {
            next.setHours(cron.hour[0]);
            next.setMinutes(cron.min[0]);
            next.setDate(next.getDate() + 1);
            continue;
        }
        // Hour
        if (!cron.hour.includes(next.getHours())) {
            next.setMinutes(cron.min[0]);
            next.setHours(next.getHours() + 1);
            continue;
        }
        // Minute
        if (!cron.min.includes(next.getMinutes())) {
            next.setMinutes(next.getMinutes() + 1);
            continue;
        }
        break;
    }
    return next;
}

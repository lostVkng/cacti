"use strict";
/*

    Clean input and output
    --> Basically the CLI client
*/
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.help = exports.remove = exports.add = exports.stop = exports.start = exports.list = void 0;
const client_1 = require("./client");
const ascii = __importStar(require("./UI/ascii_table"));
/*
    Formatting code
*/
async function list() {
    try {
        // establish client
        let _client = new client_1.Client();
        // craft message
        let msg = { command: 'list', args: [] };
        // send message
        let res = await _client.sendMsg(msg);
        // construct terminal
        let terminalString = '';
        let termWidth = process.stdout.columns;
        // active stems section
        let headers = ['name', 'cpu', 'mem', 'pid', 'time up', 'cron', 'forever'];
        let rows = res.data.active.map((x, i) => {
            return [
                x.name.toString(),
                Math.round(x.cpu) + '%', x.mem + ' MB',
                x.pid.toString(),
                timeString(x.time_up),
                x.cron.toString(),
                x.forever.toString()
            ];
        });
        terminalString += '\nActive Stems \n';
        terminalString += ascii.table(headers, rows);
        // scheduled stems section
        let sHeaders = ['name', 'cronString', 'lastRun', 'nextRun'];
        let sRows = res.data.scheduled.map((x, i) => {
            return [
                x.name.toString(),
                x.cronString,
                x.lastRun,
                x.nextRun,
            ];
        });
        terminalString += '\nScheduled Stems \n';
        terminalString += ascii.table(sHeaders, sRows);
        console.log(terminalString);
    }
    catch (error) {
        console.log(error);
    }
}
exports.list = list;
async function start(args, name, forever) {
    try {
        // establish client
        let _client = new client_1.Client();
        // craft message
        let msg = { command: 'start', args: args, name: name, forever: forever };
        // send message
        _client.sendMsg(msg)
            .then((data) => {
            if (data.error)
                errConsole(data.error);
            if (data.data)
                console.log(data);
        })
            .catch((err) => errConsole(err));
    }
    catch (err) {
        console.log(err);
    }
}
exports.start = start;
async function stop(pid) {
    try {
        // establish client
        let _client = new client_1.Client();
        // craft message
        let msg = { command: 'stop', args: [], pid: pid };
        // send message
        _client.sendMsg(msg)
            .then((data) => {
            if (data.error)
                errConsole(data.error);
            if (data.data)
                console.log(data);
        })
            .catch((err) => errConsole(err));
    }
    catch (err) {
        console.log(err);
    }
}
exports.stop = stop;
async function add(cronArgs, args, name) {
    try {
        // establish client
        let _client = new client_1.Client();
        // craft message
        let msg = { command: 'add', args: args, name: name, cron_args: cronArgs };
        // send message
        _client.sendMsg(msg)
            .then((data) => {
            if (data.error)
                errConsole(data.error);
            if (data.data)
                console.log(data);
        })
            .catch((err) => errConsole(err));
    }
    catch (err) {
        console.log(err);
    }
}
exports.add = add;
async function remove(name) {
    try {
        // establish client
        let _client = new client_1.Client();
        // craft message
        let msg = { command: 'remove', name: name, args: [] };
        // send message
        _client.sendMsg(msg)
            .then((data) => {
            if (data.error)
                errConsole(data.error);
            if (data.data)
                console.log(data);
        })
            .catch((err) => errConsole(err));
    }
    catch (err) {
        console.log(err);
    }
}
exports.remove = remove;
async function help() {
    let s = '';
    // title
    s += 'Usage: cacti <command> [optional flags] [arguments]';
    s += '\n\n';
    // commands
    s += 'cacti <server>    | launch cacti server\n';
    s += '\n';
    s += 'cacti <list>      | lists active and scheduled jobs\n';
    s += '\n';
    s += 'cacti <start> [shell arguments]     | start job\n';
    s += 'optional flags:\n';
    s += '    -name=[name]    | name to identify job\n';
    s += '    -forever        | relaunch job if it exits\n';
    s += '\n';
    s += 'cacti <stop> [pid]     | stop job\n';
    s += '\n';
    s += 'cacti <add> [cron string] [shell arguments]     | add cron job\n';
    s += 'optional flags:\n';
    s += '    -name=[name]    | name to identify job\n';
    s += '\n';
    s += 'cacti <remove> [name]     | remove cron job';
    s += '\n';
    console.log(s);
}
exports.help = help;
/*
    Helper functions
*/
function errConsole(err) {
    console.log(err);
}
function timeString(ms) {
    if (isNaN(ms))
        return 'NaN';
    if (ms < 1000) {
        return Math.round(ms) + 'ms';
    }
    else if (ms < 60000) {
        return Math.round(ms / 1000) + 's';
    }
    else if (ms < 3600000) {
        return Math.round(ms / 60000) + 'm';
    }
    else if (ms < 86400000) {
        return Math.round(ms / 3600000) + 'h';
    }
    else {
        return Math.round(ms / 86400000) + 'd';
    }
}

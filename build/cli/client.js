"use strict";
/*
    tcp client

    this is mainly just a wrapper to not have to write this over and over
*/
Object.defineProperty(exports, "__esModule", { value: true });
exports.Client = void 0;
const json_wire_1 = require("../server/json_wire");
class Client {
    constructor() {
        // create socket connection
        this.socket = new json_wire_1.JsonWire();
        this.socket.connect({ host: '127.0.0.1', port: 8127 });
    }
    sendMsg(msg) {
        let _client = this.socket;
        return new Promise((resolve, reject) => {
            // send message to server
            _client.write(msg);
            // wait for data
            _client.on('data', (res) => {
                // close the connection
                _client.end();
                // send data back
                resolve(res);
            });
            // or error
            _client.on('error', (err) => {
                // close the connection
                _client.end();
                // send data back
                reject(err);
            });
        });
    }
    stream() {
    }
}
exports.Client = Client;

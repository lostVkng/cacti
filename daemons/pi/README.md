# Create system daemon

https://www.raspberrypi.org/documentation/linux/usage/systemd.md

1) Copy and modify cacti.service file to match your user to systemd dir
```shell
sudo cp cacti.service /etc/systemd/system/cacti.service
```

I will be creating this file in my home dir and copying from there

2) attemp to start the service
```shell
sudo systemctl start cacti.service
```

3) ensure it stops correctly
```shell
sudo systemctl stop cacti.service
```

4) enable it for startup
```shell
sudo systemctl enable cacti.service
```